import { format, utcToZonedTime } from 'date-fns-tz';

/**
 * Formats dates in start time format
 * @example 7:00 am
 * @param {String} time - time in ISO format
 * @param {String} timezone
 */
export function startTimeFormatter(time: string, timeZone: string) {
  try {
    const zonedStartDate = utcToZonedTime(time, timeZone);
    return format(zonedStartDate, 'h:mm aaa', { timeZone });
  } catch (error) {
    console.error('startTimeFormatter error: ', error);
    throw error;
  }
}

/**
 * Formats dates in end time format
 * @example 3:30 pm (PST)
 * @param {String} time - time in ISO format
 * @param {String} timezone
 */
export function endTimeFormatter(time: string, timeZone: string) {
  try {
    const zonedEndDate = utcToZonedTime(time, timeZone);
    return format(zonedEndDate, 'h:mm aaa (z)', { timeZone });
  } catch (error) {
    console.error('endTimeFormatter error: ', error);
    throw error;
  }
  //
}
