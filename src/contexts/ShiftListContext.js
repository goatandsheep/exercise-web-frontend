import {
  createContext,
  // useEffect,
  // useState
} from 'react';

const ShiftListContext = createContext([]);
export default ShiftListContext;

/*
export const ShiftListProvider = ({
  children
}) => {

  const [shifts, setShifts] = useState([])

  // useEffect(() => {
  //   setShifts(state.shifts)
  // }, [state.shifts])

  return (
    <ShiftListContext.Provider value={{ shifts, setShifts }}>
      {children}
    </ShiftListContext.Provider>
  )
}
*/
