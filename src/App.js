import 'bulma/css/bulma.min.css';
import './App.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import React, {
  // useContext,
  useEffect,
  useState,
  // useEffect
} from 'react';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import ShiftListContext from './contexts/ShiftListContext';

import ListView from './pages/ListView';
import ShiftView from './pages/ShiftView';

const App = function App() {
  // const shiftListConsumer = useContext(ShiftListContext)
  const [shifts, setShifts] = useState([]);

  const fetchData = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_SERVER}/shifts`, {
      mode: 'cors',
    });
    // only returning what's important
    const shiftData = (await response.json()).data;

    // sorting by date
    shiftData.sort((val1, val2) => new Date(val1.dtstart).getTime() - new Date(val2.dtstart).getTime());
    setShifts(shiftData);
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="">
      {/* <header>
        Have some jalebi fafda. They are yummy! 😋
      </header> */}
      <ShiftListContext.Provider value={shifts}>

        <Router>
          <main className="container is-fluid">
            <Routes>
              {/* <Route exact={true} path="/" component={ListView} /> */}
              <Route path="/:shift" element={<ShiftView />} />
              <Route path="*" element={<ListView />} />
              {/* <Route render={() => (<h1>Page Not Found</h1>)} /> */}
            </Routes>
          </main>
        </Router>
      </ShiftListContext.Provider>
    </div>
  );
};

export default App;
