import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { format, utcToZonedTime } from 'date-fns-tz';
import ShiftListContext from '../contexts/ShiftListContext';

import { endTimeFormatter, startTimeFormatter } from '../utils/index.ts';

const ShiftView = function ShiftView() {
  const shifts = useContext(ShiftListContext);
  const params = useParams();
  const shift = shifts.find((val) => {
    if (val.id === params.shift) {
      return true;
    }
    return false;
  }) || {};
  let startDateString;
  let startTimeString;
  let endTimeString;
  try {
    const zonedStartDate = utcToZonedTime(shift.dtstart, shift.location.timezone);
    startDateString = format(zonedStartDate, 'EEEE - MMMM d', { timeZone: shift.location.timeZone });

    // too lazy to lowercase the am/pm. no code size increase + cleaner code
    // const startTimeConverted = Intl.DateTimeFormat('en', {
    //     timeStyle: 'short',
    //     timeZone: shift.location.timezone,
    // })
    // startTimeString = startTimeConverted.format(new Date(shift.dtstart))
    startTimeString = startTimeFormatter(shift.dtstart, shift.location.timezone);
    // const endTimeConverted = Intl.DateTimeFormat('en', {
    //     timeStyle: 'short',
    //     timeZone: shift.location.timeZone,
    //     timeZoneName: 'short',
    // })
    // endTimeString = endTimeConverted.format(new Date(shift.dtend))
    endTimeString = endTimeFormatter(shift.dtend, shift.location.timezone);
  } catch (err) {
    console.error('shift', shift.dtstart);
    console.error(err);
  }

  return (
    shift.location
      ? (
        <div>
          <div>
            <a href="/" className="icon has-text-black" aria-label="back"><i className="fas fa-arrow-left" /></a>
            <h1 className="title is-inline-block">
              {shift.slots}
              {' '}
              <span>{shift.position}</span>
            </h1>
          </div>
          <div>
            {shift.location.name}
            {' | '}
            {shift.location.address}
          </div>
          <div className="card">
            <div className="card-content">
              <h2 className="title is-5">Booking</h2>
              <div>{startDateString}</div>
              <div>
                {startTimeString}
                {' - '}
                {endTimeString}
              </div>
              <div>
                {shift.unpaidBreakDuration}
                &nbsp;mins break
              </div>
            </div>
          </div>
          <div>
            <div className="card">
              <div className="card-content">
                <h2 className="title is-5">Shifts</h2>
                <div>
                  {shift.slots}
                  &nbsp;Shifts |
                  &nbsp;
                  {shift.matched.length}
                  &nbsp;professionals matched
                </div>
              </div>
            </div>
            <div className="card">
              <ul className="card-content">
                {shift.matched.map((val) => (
                  <li className="p-3">
                    <div className="is-flex is-justify-content-flex-start">
                      <figure className="image is-64x64">
                        <img className="is-rounded has-background-white" src={val.pro.avatar || '/logo512.png'} alt={`${val.pro.firstName} ${val.pro.lastName}`} />
                      </figure>
                      <div className="level-item p-3">
                        {val.pro.firstName}
                        &nbsp;
                        {val.pro.lastName}
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="card">
            <div className="card-content">
              <h2 className="title is-5">Instructions</h2>
              <ul>
                <li>
                  <span className="icon">
                    <i className="far fa-user" />
                  </span>
                  On-site contacts:&nbsp;
                  {shift.contacts.map((contact, i) => (
                    <span>
                      {contact.name}
                      {i < shift.contacts.length - 1 ? ', ' : ''}
                    </span>
                  ))}
                </li>
                <li>
                  <span className="icon">
                    <i className="fas fa-tshirt" />
                  </span>
                  Attire:&nbsp;
                  {shift.attire.replace(/-/g, ' ')}
                </li>
                <li>
                  <span className="icon">
                    <i className="fas fa-exclamation-triangle" />
                  </span>
                  Requirements:&nbsp;
                  {shift.additionalRequirements.map((requirement, i) => (
                    <span>
                      {requirement.replace(/-/g, ' ')}
                      {i < shift.additionalRequirements.length - 1 ? ', ' : ''}
                    </span>
                  ))}
                </li>
                <li>
                  <span className="icon">
                    <i className="fas fa-info-circle" />
                  </span>
                  {shift.additionalInstructions}
                </li>
              </ul>
            </div>
          </div>
          {/* <ul>{JSON.stringify(shift)}</ul> */}
        </div>
      ) : <span>Loading...</span>);
};

export default ShiftView;
