import React, { useContext } from 'react';
import { format, utcToZonedTime } from 'date-fns-tz';
import ShiftListContext from '../contexts/ShiftListContext';
import { endTimeFormatter, startTimeFormatter } from '../utils/index.ts';

const BubbleArray = function BubbleArray({ matched }) {
  const elementArray = [];
  for (let i = 0, len = matched.length; i < len && i < 5; i += 1) {
    elementArray.push(
      <figure className="image is-32x32" style={{ marginLeft: '-10px' }}>
        <img className="is-rounded has-background-white" src={matched[i].pro.avatar || '/logo512.png'} alt={`${matched[i].pro.firstName} ${matched[i].pro.lastName}`} />
      </figure>,
    );
  }
  return elementArray.map((bubble) => <li className="is-inline-block">{bubble}</li>);
};

const ShiftPreview = function ShiftPreview({ shift }) {
  const matchQuantity = shift.matched.length;
  return (
    <a href={shift.id} className="columns card-content">
      <div className="column has-text-black has-text-weight-semibold">
        <h3 className="title is-6 has-text-weight-bold">
          <span>{shift.slots}</span>
          {' '}
          -
          {' '}
          <span className="is-capitalized">{shift.position.replace('-', ' ')}</span>
        </h3>
        <div>
          {`${startTimeFormatter(shift.dtstart, shift.location.timezone)}
          - ${endTimeFormatter(shift.dtend, shift.location.timezone)}`}
        </div>
        <br />
        <address>{shift.location.address}</address>
      </div>
      <div className="column has-text-right">
        <div>{(shift.slots > matchQuantity) ? <b className="tag is-link is-light is-normal">Open</b> : null}</div>
        <div>
          {matchQuantity}
          {' '}
          filled
        </div>
        <ul className=""><BubbleArray matched={shift.matched} /></ul>
      </div>
    </a>
  );
};

const DayGroup = function DayGroup({ date, shifts }) {
  return (
    <div>
      <div className="card has-background-white-ter">
        <h2 className="title is-5 card-header-title">{date}</h2>
      </div>
      <ul>
        {shifts.map((shift) => <li className="card mgb-small" key={shift.id}><ShiftPreview shift={shift} /></li>)}
      </ul>
    </div>
  );
};

const ListView = function ListView() {
  const shifts = useContext(ShiftListContext);

  const dateShiftObj = {};
  for (let i = 0, len = shifts.length; i < len; i += 1) {
    const startDateZoned = utcToZonedTime(shifts[i].dtstart, shifts[i].location.timezone);
    const startDateFormatted = format(startDateZoned, 'EEEE, MMMM d');
    if (!dateShiftObj[startDateFormatted]) {
      dateShiftObj[startDateFormatted] = [];
    }
    dateShiftObj[startDateFormatted].push(shifts[i]);
  }
  const dateObjKeys = Object.keys(dateShiftObj);

  return (
    <div>
      <h1 className="title">Shifts</h1>
      <ul>
        {dateObjKeys.length ? dateObjKeys.map((date) => <li key={date}><DayGroup date={date} shifts={dateShiftObj[date]} /></li>) : 'Loading...'}
      </ul>
    </div>
  );
};

export default ListView;
