# Exercise Web Frontend

## Notes

I chose to use  date-fns over moment.js because moment.js documentation states
it's a legacy project and is not maintained. Using Intl directly is another
option, but it can get messy and time consuming. Only difficulty is that it
does not share timezone string formats with moment.js, which was the defacto
standard for date formatting. Also what was tricky was that timeZone in the
browser was timezone in the API.

I noticed there are no language strings which could be difficult for multi-
lingual jobs especially in Quebec.
